let counter = 0;

function sum(a, b) {
  console.log(counter++);
  return a + b;
}

module.exports.sum = sum;
module.exports.counter = counter;