(() => {
  const cache = {};

  const module = {
    exports: {}
  };

  function getFileByHttp(url) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.send();

    if (xhr.status >= 400) {
      return new Error(`${xhr.status}: cannot find module '${url}'`);
    }

    return xhr.response;
  }

  function getAbsolutePathFromHTML(relativePath) {
    const link = document.createElement('a');
    link.href = relativePath;

    return link.href;
  }

  const getFile = getFileByHttp;
  const getAbsolutePath = getAbsolutePathFromHTML;

  function require(path) {
    const absolutePath = getAbsolutePath(path);

    if (absolutePath in cache) {
      module.exports = cache[absolutePath];

      return module.exports;
    }

    const file = getFile(path);

    if (file instanceof Error) {
      throw file;
    }

    const moduleFn = new Function('module', 'exports', file);
    moduleFn(module, module.exports);

    cache[absolutePath] = module.exports;

    return module.exports;
  }

  require.cache = cache;

  window.require = require;
})();
